package com.mciseries.ideathrun.system;

import java.util.ArrayList;
import java.util.HashMap;

import org.bukkit.ChatColor;

public class Vars {
	public static String chatPrefix = ChatColor.GRAY + "[" + ChatColor.GOLD + "DeathRun" + ChatColor.GRAY + "]" + ChatColor.RESET + " ";
	public static HashMap<String, String> dead = new HashMap<String, String>(), respawnWithGame = new HashMap<String, String>(),
			oneTimeUseSelectors = new HashMap<String, String>(), bowButtonSelectors = new HashMap<String, String>(),
					swordButtonSelectors = new HashMap<String, String>();
	public static HashMap<String, Thread> countDowns = new HashMap<String, Thread>();
	public static ArrayList<String> deadPlayers = new ArrayList<String>(), gamesWithTeleportedActivators = new ArrayList<String>(),
			oneTimeUseUses = new ArrayList<String>();
}
