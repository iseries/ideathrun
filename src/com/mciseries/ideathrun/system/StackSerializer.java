package com.mciseries.ideathrun.system;

import org.bukkit.inventory.ItemStack;

public class StackSerializer {
        public static String serialize(ItemStack stack) {
                return stack.getTypeId() + ";" + stack.getAmount() + ";" + stack.getDurability() + ";" + stack.getItemMeta().getDisplayName();
        }
}