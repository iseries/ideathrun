package com.mciseries.ideathrun.system.files;

import java.io.File;
import java.io.IOException;

import org.bukkit.GameMode;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.mciseries.ideathrun.Main;
import com.mciseries.ideathrun.system.StackSerializer;

public class Inventories {
	private static File conFile = new File("plugins/iDeathRun/Inventories.yml");
	private static FileConfiguration conf = YamlConfiguration.loadConfiguration(conFile);
	
	public static void init() {
		try {
			if(!conFile.exists())
				conFile.createNewFile();
		} catch (IOException e) {
			Main.callError("Failed to create Inventories.yml! Try setting 'plugins' folder to chmod 777!");
		}
	}
	
	public static void addGM(Player player) {
		conf.set("Players." + player.getName() + ".Mode", "" + player.getGameMode().getValue()); save();
	}
	
	public static void add(String player, int slot, ItemStack stack) {
		if(stack == null) {
			conf.set("Players." + player + "." + slot, "none"); save();
		}
		else {
			conf.set("Players." + player + "." + slot, StackSerializer.serialize(stack)); save();
		}
	}
	
	public static void remove(String player) {
		conf.set("Players." + player, null); save();
	}
	
	public static int getGM(String player) {
		return Integer.parseInt(conf.getString("Players." + player + ".Mode"));
	}
	
	public static ItemStack get(String player, int slot) {
		if(conf.getString("Players." + player + "." + slot).equalsIgnoreCase("none"))
			return null;
		
		String[] meta = conf.getString("Players." + player + "." + slot).split("\\;");
		
		int id = Integer.parseInt(meta[0]), amount = Integer.parseInt(meta[1]);
		short durability = Short.parseShort(meta[2]);
		ItemStack stack = new ItemStack(id, amount, durability);
		ItemMeta iMeta = stack.getItemMeta();
		iMeta.setDisplayName(meta[3]);
		stack.setItemMeta(iMeta);
		
		return stack;
	}
	
	public static void restoreInv(Player p) {
		for(int i = 0; i <= 35; i++) {
			p.getInventory().setItem(i, get(p.getName(), i));
		}
		p.setGameMode(GameMode.getByValue(getGM(p.getName())));
		remove(p.getName());
	}
	
	public static void saveInv(CommandSender s, boolean changeToAdventure) {
		Player p = (Player) s;
		
		addGM(p);
		
		if(changeToAdventure)
			p.setGameMode(GameMode.ADVENTURE);
		else
			p.setGameMode(GameMode.CREATIVE);
		
		int i = 0;
		for(ItemStack stack : p.getInventory().getContents()) {
			add(p.getName(), i, stack);
			i++;
		}
		
		p.getInventory().clear();
	}
	
	private static void save() {
		try {
			conf.save(conFile);
		} catch (IOException e) {
			Main.callError("Failed to save Inventories.yml! Try setting 'plugins' folder to chmod 777!");
		}
	}
}