package com.mciseries.ideathrun.system.files;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import com.mciseries.ideathrun.Main;

public class Maps {
	private static File conFile = new File("plugins/iDeathRun/Maps.yml"), folder = new File("plugins/iDeathRun");
	private static FileConfiguration conf = YamlConfiguration.loadConfiguration(conFile);
	
	public static void init() {
		try {
			if(!folder.exists())
				folder.mkdirs();
			if(!conFile.exists())
				conFile.createNewFile();
		} catch (IOException e) {
			Main.callError("Failed to create Maps.yml! Try setting 'plugins' folder to chmod 777!");
		}
	}
	
	public static String[] mapList() {
		String[] empty = {};
		
		if(conf.getConfigurationSection("Maps") == null)
			return empty;
		else
			return conf.getConfigurationSection("Maps").getKeys(false).toArray(new String[conf.getConfigurationSection("Maps").getKeys(false).size()]);
	}
	
	public static void clear(String map, String key) {
		List<String> list = conf.getStringList("Maps." + map.toLowerCase() + "." + key);
		list.clear();
		
		conf.set("Maps." + map.toLowerCase() + "." + key, list);
	}
	
	public static void delete(String map) {
		conf.set("Maps." + map.toLowerCase(), null); save();
	}
	
	public static void create(String map) {
		conf.set("Maps." + map.toLowerCase() + ".DisplayName", map); save();
	}
	
	public static Location getLocation(String map, String key) {
        String[] locVal = conf.getString("Maps." + map.toLowerCase() + "." + key).split("\\;");
        Location loc = new Location(Bukkit.getWorld(locVal[0]), Double.parseDouble(locVal[1]), Double.parseDouble(locVal[2]), Double.parseDouble(locVal[3]), Float.parseFloat(locVal[5]), Float.parseFloat(locVal[4]));
        
        return loc;
	}
	
	public static String[] listArray(String map, String key) {
		return conf.getStringList("Maps." + map.toLowerCase() + "." + key).toArray(new String[conf.getStringList("Maps." + map.toLowerCase() + "." + key).size()]);
	}
	
	public static int length(String map, String key) {
		return conf.getStringList("Maps." + map.toLowerCase() + "." + key).size();
	}
	
	public static void set(String map, String key, String val) {
		conf.set("Maps." + map.toLowerCase() + "." + key, val); save();
	}
	
	public static String get(String map, String key) {
		return conf.getString("Maps." + map.toLowerCase() + "." + key);
	}
	
	public static void add(String map, String key, String val) {
		List<String> list = conf.getStringList("Maps." + map.toLowerCase() + "." + key);
		list.add(val);
		
		conf.set("Maps." + map.toLowerCase() + "." + key, list); save();
	}
	
	public static void remove(String map, String key, String val) {
		List<String> list = conf.getStringList("Maps." + map.toLowerCase() + "." + key);
		list.remove(val);
		
		conf.set("Maps." + map.toLowerCase() + "." + key, list); save();
	}
	
	public static boolean contains(String map, String key, String val) {
		return conf.getStringList("Maps." + map.toLowerCase() + "." + key).contains(val);
	}
	
	private static void save() {
		try {
			conf.save(conFile);
		} catch (IOException e) {
			Main.callError("Failed to save Maps.yml! Try setting 'plugins' folder to chmod 777!");
		}
	}
}
