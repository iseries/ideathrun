package com.mciseries.ideathrun.system.files;

import java.io.File;
import java.io.IOException;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import com.mciseries.ideathrun.Main;

public class Config {
	private static File conFile = new File("plugins/iDeathRun/Config.yml");
	private static FileConfiguration conf = YamlConfiguration.loadConfiguration(conFile);
	
	public static boolean debug, sendStats, scoreBoard, dedicatedMode;
	public static int autoStart, floorId, activatorWeapon, maxPlayers;
	public static double activatorAward;
	public static String cmdOnRoundEnd, bungeeServerOnRoundEnd;
	
	public static void init() {
		try {
			if(!conFile.exists())
				conFile.createNewFile();
		} catch (IOException e) {
			Main.callError("Failed to create Config.yml! Try setting 'plugins' folder to chmod 777!");
		}
		
		setDefault("ActivatorMoneyAward", 0.0D); activatorAward = conf.getDouble("ActivatorMoneyAward");
		setDefault("SendStats", true); sendStats = conf.getBoolean("SendStats");
		setDefault("PlayersUntilAutoStart", 3); autoStart = conf.getInt("PlayersUntilAutoStart");
		setDefault("MaxPlayersPerArena", 7); maxPlayers = conf.getInt("MaxPlayersPerArena");
		setDefault("MapFloorBlockId", 35); floorId = conf.getInt("MapFloorBlockId");
		setDefault("Debug", false); debug = conf.getBoolean("Debug");
		setDefault("MapInfoOnScoreboard", false); scoreBoard = conf.getBoolean("MapInfoOnScoreboard");
		setDefault("DedicatedMode", false); dedicatedMode = conf.getBoolean("DedicatedMode");
		setDefault("CmdOnRoundEnd", "/example"); cmdOnRoundEnd = conf.getString("CmdOnRoundEnd");
		setDefault("BungeeServerOnRoundEnd", "none"); bungeeServerOnRoundEnd = conf.getString("BungeeServerOnRoundEnd");
	}
	
	
	private static void setDefault(String key, Object val) {
		if(conf.getString(key) == null) {
			conf.set(key, val);
			save();
		}
	}
	private static void save() {
		try {
			conf.save(conFile);
		} catch (IOException e) {
			Main.callError("Failed to save Config.yml! Try setting 'plugins' folder to chmod 777!");
		}
	}
}
