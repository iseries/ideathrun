package com.mciseries.ideathrun.system.extras;

import org.bukkit.entity.Player;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import com.mciseries.ideathrun.Main;
import com.mciseries.ideathrun.system.files.Config;

public class Bungee {
	public static void send(Player p) {
		ByteArrayDataOutput output = ByteStreams.newDataOutput();
		output.writeUTF("Connect");
		output.writeUTF(Config.bungeeServerOnRoundEnd);
		p.sendPluginMessage(Main.instance, "BungeeCord", output.toByteArray());
	}
}
