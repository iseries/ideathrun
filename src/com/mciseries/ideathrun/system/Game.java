package com.mciseries.ideathrun.system;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Server;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

import com.mciseries.ideathrun.system.extras.Bungee;
import com.mciseries.ideathrun.system.files.Config;
import com.mciseries.ideathrun.system.files.Inventories;
import com.mciseries.ideathrun.system.files.Maps;

public class Game {
	private String arena;
	private String name;
	private Server serv;
	
	public Game(String arena) {
		this.arena = arena.toLowerCase();
		this.name = Maps.get(this.arena, "DisplayName");
		this.serv = Bukkit.getServer();
	}
	
	public void delete() {
		Maps.delete(arena);
	}
	
	public boolean playerIsPlaying(String player) {
		return Maps.contains(arena, "PlayersInGame", player);
	}
	
	public String getName() {
		return arena;
	}
	
	public String getActivator() {
		return Maps.get(arena, "Activator");
	}
	
	public String getDisplayName() {
		return name;
	}
	
	public void setDisplayName(String displayName) {
		Maps.set(arena, "DisplayName", displayName);
		name = displayName;
	}
	
	public void create() {
		Maps.create(arena);
	}
	
	public void playerJoin(Player p) {
		Maps.add(arena, "PlayersInGame", p.getName());
		Inventories.saveInv(p, true);
		p.teleport(getHub());
		p.setHealth(20);
		
		if(Maps.length(arena, "PlayersInGame") == Config.autoStart) {
			send(ChatColor.AQUA + p.getDisplayName() + ChatColor.RESET + " joined the game! (Round starting!)");
			startCountdown();
		}
		else {
			send(ChatColor.AQUA + p.getDisplayName() + ChatColor.RESET + " joined the game! (" + ChatColor.AQUA + "#" + Maps.length(arena, "PlayersInGame") + "/" + Config.autoStart + ChatColor.RESET + ")");
		}
		Scoreboard.reCount();
	}
	
	public void setOneTimeUse(Location loc) {
		Maps.add(arena, "OneTimeUseButtons", Utils.serializeBlockLocation(loc));
	}
	
	public boolean isOneTimeUse(Location loc) {
		return Maps.contains(arena, "OneTimeUseButtons", Utils.serializeBlockLocation(loc));
	}
	
	public Thread getCountDown() {
		return Vars.countDowns.get(arena);
	}
	
	public boolean isSwordButton(Location loc) {
		return Maps.get(arena, "SwordButton").equalsIgnoreCase(Utils.serializeBlockLocation(loc));
	}
	
	public boolean isBowButton(Location loc) {
		return Maps.get(arena, "BowButton").equalsIgnoreCase(Utils.serializeBlockLocation(loc));
	}
	
	public void setBowButton(Location loc) {
		Maps.set(arena, "BowButton", Utils.serializeBlockLocation(loc));
	}
	
	public void setSwordButton(Location loc) {
		Maps.set(arena, "SwordButton", Utils.serializeBlockLocation(loc));
	}
	
	public Location getRoomsSpawn() {
		return Maps.getLocation(arena, "RoomsSpawn");
	}
	
	public Location getActivatorRoomSpawn() {
		return Maps.getLocation(arena, "ActivatorRoomSpawn");
	}
	
	public void send(String message) {
		for(String player : Maps.listArray(arena, "PlayersInGame")) {
			serv.getPlayer(player).sendMessage(Vars.chatPrefix + message);
		}
	}
	
	public Location getHub() {
		return Maps.getLocation(arena, "Hub");
	}
	
	public Location getRunnerSpawn() {
		return Maps.getLocation(arena, "RunnerSpawn");
	}
	
	public Location getActivatorSpawn() {
		return Maps.getLocation(arena, "ActivatorSpawn");
	}
	
	public void setRoomsSpawn(Location loc) {
		Maps.set(arena, "RoomsSpawn", loc.getWorld().getName() + ";" + loc.getX() + ";" + loc.getY() + ";" + loc.getZ() + ";" + loc.getPitch() + ";" + loc.getYaw());
	}
	
	public void setActivatorRoomSpawn(Location loc) {
		Maps.set(arena, "ActivatorRoomSpawn", loc.getWorld().getName() + ";" + loc.getX() + ";" + loc.getY() + ";" + loc.getZ() + ";" + loc.getPitch() + ";" + loc.getYaw());
	}
	
	public void setHub(Location loc) {
		Maps.set(arena, "Hub", loc.getWorld().getName() + ";" + loc.getX() + ";" + loc.getY() + ";" + loc.getZ() + ";" + loc.getPitch() + ";" + loc.getYaw());
	}
	
	public void setRunnerSpawn(Location loc) {
		Maps.set(arena, "RunnerSpawn", loc.getWorld().getName() + ";" + loc.getX() + ";" + loc.getY() + ";" + loc.getZ() + ";" + loc.getPitch() + ";" + loc.getYaw());
	}
	
	public void setActivatorSpawn(Location loc) {
		Maps.set(arena, "ActivatorSpawn", loc.getWorld().getName() + ";" + loc.getX() + ";" + loc.getY() + ";" + loc.getZ() + ";" + loc.getPitch() + ";" + loc.getYaw());
	}
	
	public boolean isStarted() {
		if(Maps.get(arena, "Started") == null)
			return false;
		
		return Maps.get(arena, "Started").equalsIgnoreCase("Y");
	}
	
	public void setStarted(boolean started) {
		if(started) {
			Maps.set(arena, "Started", "Y");
		}
		else {
			Maps.set(arena, "Started", "n");
			for(Player p : serv.getOnlinePlayers()) {
				p.playSound(p.getLocation(), Sound.LEVEL_UP, 2, 0);
			}
		}
	}
	
	public void startCountdown() {
		Thread t = new Thread(new Countdown(arena));
		Vars.countDowns.put(arena, t);
		t.start();
	}
	
	public void xp(int level) {
		for(String player : Maps.listArray(arena, "PlayersInGame")) {
			Bukkit.getPlayer(player).setLevel(level);
		}
	}
	
	public void start() {
		int min = 0, max = Maps.length(arena, "PlayersInGame") - 1, key = min + (int)(Math.random() * ((max - min) + 1));
		String[] playerList = Maps.listArray(arena, "PlayersInGame");
		String activator = playerList[key];
		Player act = serv.getPlayer(activator);
		Maps.set(arena, "Activator", activator);
		setStarted(true);
		
		send(ChatColor.AQUA + act.getDisplayName() + ChatColor.RESET + " is the activator!");
		for(String player : Maps.listArray(arena, "PlayersInGame")) {
			if(!player.equalsIgnoreCase(Maps.get(arena, activator)))
				serv.getPlayer(player).teleport(getRunnerSpawn());
			
			serv.getPlayer(player).playSound(serv.getPlayer(player).getLocation(), Sound.WITHER_SPAWN, 2, 2);
		}
		
		act.teleport(getActivatorSpawn());
		
	}
	
	public void playerLeave(Player p, String cause) {
		Maps.remove(arena, "PlayersInGame", p.getName());
		int runners = Maps.length(arena, "PlayersInGame") - 1;
		for(Player player : serv.getOnlinePlayers()) {
			if(playerIsPlaying(player.getName())) {
				player.playSound(p.getLocation(), Sound.WITHER_HURT, 2, 0);
			}
		}
		
		if(!cause.contains("die")) {
			p.setHealth(20);
			p.setFoodLevel(20);
			Inventories.restoreInv(p);
			p.teleport(getHub());
		}
		p.sendMessage(ChatColor.AQUA + "You have " + cause + "! " + ChatColor.AQUA + runners + ChatColor.RESET + " runner(s) are left.");
		
		if(isStarted()) {
			if(!p.getName().equalsIgnoreCase(Maps.get(arena, "Activator"))) {
				if(runners > 0) {
					send(ChatColor.AQUA + p.getDisplayName() + ChatColor.RESET + " has " + cause + "! " + ChatColor.AQUA + runners + ChatColor.RESET + " runner(s) are left.");
				}
				else {
					Maps.clear(arena, "PlayersInGame");
					Player activator = serv.getPlayer(Maps.get(arena, "Activator"));
					
					if(Config.activatorAward > 0.0D) {
						Vault.deposit(activator, Config.activatorAward);
						activator.sendMessage(Vars.chatPrefix + "You have been awarded " + ChatColor.AQUA + Config.activatorAward + " " + Vault.name(Config.activatorAward) + " " + ChatColor.RESET + "for winning DeathRun!");
					}
					
					if(Config.sendStats) {
						Stats.submitActivator(true);
						Stats.submitWinner(Maps.get(arena, "Activator"));
					}
					
					serv.broadcastMessage(Vars.chatPrefix + ChatColor.AQUA + p.getDisplayName() + ChatColor.RESET + " has " + cause + "! The activator, " + ChatColor.AQUA + activator.getDisplayName() + ChatColor.RESET + ", wins on arena " + ChatColor.AQUA  + name + ChatColor.RESET + "!");
					
					
					Maps.set(arena, "Activator", null);
					Inventories.restoreInv(activator);
					activator.teleport(getHub());
					setStarted(false);
					Vars.gamesWithTeleportedActivators.remove(arena);
					Vars.oneTimeUseUses.clear();
					if(!Config.cmdOnRoundEnd.equalsIgnoreCase("/example")) {
						for(Player pl : Bukkit.getOnlinePlayers()) {
							pl.performCommand(Config.cmdOnRoundEnd.replace("/", ""));
						}
					}
					if(!Config.bungeeServerOnRoundEnd.equalsIgnoreCase("none")) {
						for(Player pl : Bukkit.getOnlinePlayers()) {
							Bungee.send(pl);
						}
					}
				}
			}
			else {
				for(Player runner : serv.getOnlinePlayers()) {
					if(playerIsPlaying(runner.getName())) {
						Maps.remove(arena, "PlayersInGame", runner.getName());
						Inventories.restoreInv(runner);
						runner.teleport(getHub());
					}
				}
				
				serv.broadcastMessage(Vars.chatPrefix + "The activator, " + ChatColor.AQUA + p.getDisplayName() + ChatColor.RESET + ", has " + cause + "! The runners win on arena " + ChatColor.AQUA + name + ChatColor.RESET + "!");
				if(Config.sendStats) {
					Stats.submitActivator(false);
				}
				Maps.set(arena, "Activator", null);
				setStarted(false);
				Vars.gamesWithTeleportedActivators.remove(arena);
				Vars.oneTimeUseUses.clear();
				if(!Config.cmdOnRoundEnd.equalsIgnoreCase("/example")) {
					for(Player pl : Bukkit.getOnlinePlayers()) {
						pl.performCommand(Config.cmdOnRoundEnd.replace("/", ""));
					}
				}
				if(!Config.bungeeServerOnRoundEnd.equalsIgnoreCase("none")) {
					for(Player pl : Bukkit.getOnlinePlayers()) {
						Bungee.send(pl);
					}
				}
			}
		}
		else {
			if(Maps.length(arena, "PlayersInGame") < Config.autoStart && Vars.countDowns.containsKey(arena)) {
				Vars.countDowns.get(arena).interrupt();
				xp(0);
				Vars.countDowns.remove(arena);
			}
			
			send(ChatColor.AQUA + p.getDisplayName() + ChatColor.RESET + " has " + cause + "! (" + ChatColor.AQUA + "#" + Maps.length(arena, "PlayersInGame") + "/" + Config.autoStart + ChatColor.RESET + ")");
		}
		Scoreboard.reCount();
	}
}
