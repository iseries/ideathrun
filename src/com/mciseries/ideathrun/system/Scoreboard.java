package com.mciseries.ideathrun.system;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.ScoreboardManager;

import com.mciseries.ideathrun.system.files.Config;
import com.mciseries.ideathrun.system.files.Maps;

public class Scoreboard {
	public static void init() {
		if(!Config.scoreBoard) { return; }
		ScoreboardManager manager = Bukkit.getScoreboardManager();
		org.bukkit.scoreboard.Scoreboard board = manager.getNewScoreboard();
		
		Objective objective = board.registerNewObjective("maps", "dummy");
		objective.setDisplaySlot(DisplaySlot.SIDEBAR);
		objective.setDisplayName("Death Run Maps");
		
		for(Player p : Bukkit.getOnlinePlayers()) {
			for(String arena : Maps.mapList()) {
				Game game = new Game(arena);
				
				Score score = objective.getScore(Bukkit.getOfflinePlayer(game.getDisplayName() + ":"));
				score.setScore(Maps.length(game.getName(), "PlayersInGame"));
			}
			
			p.setScoreboard(board);
		}
	}
	
	public static void reCount() {
		if(!Config.scoreBoard) { return; }
		for(Player p : Bukkit.getOnlinePlayers()) {
			org.bukkit.scoreboard.Scoreboard playerBoard = p.getScoreboard();
			if(playerBoard != null) {
				Objective objective = playerBoard.getObjective("maps");
				
				for(String arena : Maps.mapList()) {
					Game game = new Game(arena);
					
					Score score = objective.getScore(Bukkit.getOfflinePlayer(game.getDisplayName() + ":"));
					score.setScore(Maps.length(game.getName(), "PlayersInGame"));
				}
			}
		}
	}
}
