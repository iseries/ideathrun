package com.mciseries.ideathrun.system;

public class Countdown implements Runnable {
	String name;
	Game game;
	
	public Countdown(String name) {
		this.name = name;
		this.game = new Game(name);
	}
	
	@Override
	public void run() {
		try {
			game.xp(45);
			Thread.sleep(1000);
			game.xp(44);
			Thread.sleep(1000);
			game.xp(43);
			Thread.sleep(1000);
			game.xp(42);
			Thread.sleep(1000);
			game.xp(41);
			Thread.sleep(1000);
			game.xp(40);
			Thread.sleep(1000);
			game.xp(39);
			Thread.sleep(1000);
			game.xp(38);
			Thread.sleep(1000);
			game.xp(37);
			Thread.sleep(1000);
			game.xp(36);
			Thread.sleep(1000);
			game.xp(35);
			Thread.sleep(1000);
			game.xp(34);
			Thread.sleep(1000);
			game.xp(33);
			Thread.sleep(1000);
			game.xp(32);
			Thread.sleep(1000);
			game.xp(31);
			Thread.sleep(1000);
			game.xp(30);
			Thread.sleep(1000);
			game.xp(29);
			Thread.sleep(1000);
			game.xp(28);
			Thread.sleep(1000);
			game.xp(27);
			Thread.sleep(1000);
			game.xp(26);
			Thread.sleep(1000);
			game.xp(25);
			Thread.sleep(1000);
			game.xp(24);
			Thread.sleep(1000);
			game.xp(23);
			Thread.sleep(1000);
			game.xp(22);
			Thread.sleep(1000);
			game.xp(21);
			Thread.sleep(1000);
			game.xp(20);
			Thread.sleep(1000);
			game.xp(19);
			Thread.sleep(1000);
			game.xp(18);
			Thread.sleep(1000);
			game.xp(17);
			Thread.sleep(1000);
			game.xp(16);
			Thread.sleep(1000);
			game.xp(15);
			Thread.sleep(1000);
			game.xp(14);
			Thread.sleep(1000);
			game.xp(13);
			Thread.sleep(1000);
			game.xp(12);
			Thread.sleep(1000);
			game.xp(11);
			Thread.sleep(1000);
			game.xp(10);
			Thread.sleep(1000);
			game.xp(9);
			Thread.sleep(1000);
			game.xp(8);
			Thread.sleep(1000);
			game.xp(7);
			Thread.sleep(1000);
			game.xp(6);
			Thread.sleep(1000);
			game.xp(5);
			Thread.sleep(1000);
			game.xp(4);
			Thread.sleep(1000);
			game.xp(3);
			Thread.sleep(1000);
			game.xp(2);
			Thread.sleep(1000);
			game.xp(1);
			Thread.sleep(1000);
			game.xp(0);
			game.start();
		} catch (InterruptedException e) {}
	}
}
