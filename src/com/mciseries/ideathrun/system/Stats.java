package com.mciseries.ideathrun.system;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Stats {
	public static void submitActivator(boolean activator) {
		submit("http://stats.rtaink.com/ideathrun?activator=" + activator);
	}
	
	public static void submitWinner(String winner) {
		submit("http://stats.rtaink.com/ideathrun?winner=" + winner);
	}
	
	private static void submit(String statUrl) {
		try {
			URL url = new URL(statUrl);
			
			URLConnection conn = url.openConnection();
			InputStream is = conn.getInputStream();
			
			is.close();
		} catch (MalformedURLException e) {
			Logger.getLogger("Minecraft").log(Level.INFO, "rtainc stats servers are down :(. This will not affect plugin functionality.");
		} catch (IOException e) {}
	}
}