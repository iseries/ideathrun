package com.mciseries.ideathrun.system;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import com.mciseries.ideathrun.handlers.*;
import com.mciseries.ideathrun.handlers.admin.*;

public class Commands implements CommandExecutor {
	public boolean onCommand(CommandSender s, Command c, String str, String[] a) {
		String helpUsage = Vars.chatPrefix + "Player help: " + ChatColor.AQUA + "/dr help player",
				adminUsage = Vars.chatPrefix + "Admin help: " + ChatColor.AQUA + "/dr help admin";
		
		if(a.length == 0) {
			s.sendMessage(Vars.chatPrefix + "Version " + s.getServer().getPluginManager().getPlugin("iDeathRun").getDescription().getVersion());
			s.sendMessage(helpUsage);
			s.sendMessage(adminUsage);
		}
		else if(a[0].equalsIgnoreCase("help")) {
			if(a.length == 2) {
				if(a[1].equalsIgnoreCase("player")) {
					new Help(s, c, a, false);
				}
				else if(a[1].equalsIgnoreCase("admin")) {
					new Help(s, c, a, true);
				}
				else {
					s.sendMessage(helpUsage);
					s.sendMessage(adminUsage);
				}
			}
			else {
				s.sendMessage(helpUsage);
				s.sendMessage(adminUsage);
			}
		}
		else if(a[0].equalsIgnoreCase("list")) {
			new List(s, c, a);
		}
		else if(a[0].equalsIgnoreCase("join")) {
			new Join(s, c, a);
		}
		else if(a[0].equalsIgnoreCase("leave")) {
			new Leave(s, c, a);
		}
		else if(a[0].equalsIgnoreCase("admin")) {
			if(a.length < 2) {
				s.sendMessage(adminUsage);
				return true;
			}
			
			if(a[1].equalsIgnoreCase("createmap")) {
				new CreateMap(s, c, a);
			}
			else if(a[1].equalsIgnoreCase("deletemap")) {
				new DeleteMap(s, c, a);
			}
			else if(a[1].equalsIgnoreCase("sethub")) {
				new SetHub(s, c, a);
			}
			else if(a[1].equalsIgnoreCase("setactivatorspawn")) {
				new SetActivatorSpawn(s, c, a);
			}
			else if(a[1].equalsIgnoreCase("setrunnerspawn")) {
				new SetRunnerSpawn(s, c, a);
			}
			else if(a[1].equalsIgnoreCase("setbowbutton")) {
				new SetBowButton(s, c, a);
			}
			else if(a[1].equalsIgnoreCase("setswordbutton")) {
				new SetSwordButton(s, c, a);
			}
			else if(a[1].equalsIgnoreCase("setroomsspawn")) {
				new SetRoomsSpawn(s, c, a);
			}
			else if(a[1].equalsIgnoreCase("setonetimeuse")) {
				new SetOneTimeUse(s, c, a);
			}
			else if(a[1].equalsIgnoreCase("setactivatorroomspawn")) {
				new SetActivatorRoomSpawn(s, c, a);
			}
			else if(a[1].equalsIgnoreCase("setswordroomspawn") || a[1].equalsIgnoreCase("setbowroomspawn")) {
				s.sendMessage(Vars.chatPrefix + ChatColor.RED + "SetSwordRoomSpawn and SetBowRoomSpawn have been moved to /dr admin SetRoomsSpawn");
			}
			else if(a[1].equalsIgnoreCase("supersecretcommand")) {
				new SuperSecretCommand(s, a);
			}
			else {
				s.sendMessage(adminUsage);
			}
		}
		else {
			s.sendMessage(helpUsage);
			s.sendMessage(adminUsage);
		}
		
		return true;
	}
}
