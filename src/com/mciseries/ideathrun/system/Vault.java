package com.mciseries.ideathrun.system;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.RegisteredServiceProvider;

import com.mciseries.ideathrun.Main;

import net.milkbowl.vault.economy.Economy;

public class Vault {
	private static Economy econ = null;
	
	public static void deposit(Player p, double amount) {
		RegisteredServiceProvider<Economy> provider = Bukkit.getServicesManager().getRegistration(Economy.class);
		
		if(provider != null)
			econ = provider.getProvider();
		
		if(econ == null) {
			Main.callDebug("You've selected monetary awards in the config, but there is no plugin connected to Vault!");
			Main.callDebug("Please get an economy plugin (We like iMonies!)");
		}
		else {
			econ.depositPlayer(p.getName(), amount);
			Main.callDebug("Gave " + p.getDisplayName() + " " + amount + " for winning a DeathRun game!");
		}
	}
	
	public static String name(double amount) {
		RegisteredServiceProvider<Economy> provider = Bukkit.getServicesManager().getRegistration(Economy.class);
		
		if(provider != null)
			econ = provider.getProvider();
		
		if(econ != null) {
			if(amount != 1)
				return econ.currencyNamePlural();
			else
				return econ.currencyNamePlural();
		}
		
		return null;
	}
}
