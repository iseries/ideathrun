package com.mciseries.ideathrun.system;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import com.mciseries.ideathrun.system.files.Maps;

public class Utils {
	public static String getMapPlayerIsIn(Player p) {
		for(String map : Maps.mapList()) {
			Game game = new Game(map);
			if(game.playerIsPlaying(p.getName()))
				return game.getName();
		}
		
		return null;
	}
	
	public static String serializeBlockLocation(Location loc) {
		return loc.getWorld().getName() + ";" + loc.getBlockX() + ";" + loc.getBlockY() + ";" + loc.getBlockZ();
	}
	
	public static Location deserializeBlockLocation(String str) {
		String[] loc = str.split("\\;");
		
		return new Location(Bukkit.getWorld(loc[0]), Integer.parseInt(loc[1]), Integer.parseInt(loc[2]), Integer.parseInt(loc[3]));
	}
}
