package com.mciseries.ideathrun;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import com.mciseries.ideathrun.listeners.*;
import com.mciseries.ideathrun.system.Commands;
import com.mciseries.ideathrun.system.Scoreboard;
import com.mciseries.ideathrun.system.extras.Metrics;
import com.mciseries.ideathrun.system.files.Config;
import com.mciseries.ideathrun.system.files.Inventories;
import com.mciseries.ideathrun.system.files.Maps;

public class Main extends JavaPlugin {
	public static Main instance;
	
	public Main() {
		instance = this;
	}
	
	public void onEnable() {
		Bukkit.getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");
		
		getCommand("deathrun").setExecutor(new Commands());
		
		getServer().getPluginManager().registerEvents(new Death(), this);
		getServer().getPluginManager().registerEvents(new Movements(), this);
		getServer().getPluginManager().registerEvents(new Respawn(), this);
		getServer().getPluginManager().registerEvents(new ServerLeaving(), this);
		getServer().getPluginManager().registerEvents(new Interactions(), this);
		getServer().getPluginManager().registerEvents(new Hurt(), this);
		getServer().getPluginManager().registerEvents(new Building(), this);
		getServer().getPluginManager().registerEvents(new ServerJoining(), this);
		getServer().getPluginManager().registerEvents(new ServerKick(), this);
		
		Maps.init();
		Config.init();
		Inventories.init();
		
		
		Scoreboard.init();
		try {
			Metrics m = new Metrics(this);
			m.start();
		} catch (IOException e) {
			getLogger().warning("Submitting of ze stats failed... either you're opted out, or MCStats.org is down. This will not affect usability.");
		}
		
	}
	
	
	public static void callError(String msg) {
		Logger.getLogger("Minecraft").log(Level.SEVERE, "[iDeathRun Error - Report to tickets.rtaink.com/ideathrun] " + msg);
	}
	
	public static void callDebug(String msg) {
		if(Config.debug) {
			Logger.getLogger("Minecraft").log(Level.SEVERE, "[iDeathRun Debug] " + msg);
		}
	}
}
