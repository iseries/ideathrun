package com.mciseries.ideathrun.handlers.admin;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import com.mciseries.ideathrun.system.Game;
import com.mciseries.ideathrun.system.Vars;
import com.mciseries.ideathrun.system.files.Maps;

public class CreateMap {
	public CreateMap(CommandSender s, Command c, String[] a) {
		if(!s.hasPermission("ideathrun.admin")) {
			s.sendMessage(Vars.chatPrefix + ChatColor.DARK_RED + "You do not have permission for this!");
			return;
		}
		if(a.length < 3) {
			s.sendMessage(Vars.chatPrefix + ChatColor.RED + "Usage: /dr admin createmap <map>");
			return;
		}
		Game game = new Game(a[2]);
		if(game.getDisplayName() != null) {
			s.sendMessage(Vars.chatPrefix + ChatColor.RED + "The map " + ChatColor.AQUA + a[2] + ChatColor.RED + " already exists! Either choose a new name, or delete this one.");
			return;
		}
		
		Maps.set(a[2].toLowerCase(), "DisplayName", a[2]);
		s.sendMessage(Vars.chatPrefix + "Map " + ChatColor.AQUA + a[2] + ChatColor.RESET + " created!");
	}
}
