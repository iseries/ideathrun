package com.mciseries.ideathrun.handlers.admin;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import com.mciseries.ideathrun.system.Game;
import com.mciseries.ideathrun.system.Vars;

public class DeleteMap {
	public DeleteMap(CommandSender s, Command c, String[] a) {
		if(!s.hasPermission("ideathrun.admin")) {
			s.sendMessage(Vars.chatPrefix + ChatColor.DARK_RED + "You do not have permission for this!");
			return;
		}
		if(a.length < 3) {
			s.sendMessage(Vars.chatPrefix + ChatColor.RED + "Usage: /dr admin deletemap <map>");
			return;
		}
		Game game = new Game(a[2]);
		if(game.getDisplayName() == null) {
			s.sendMessage(Vars.chatPrefix + ChatColor.RED + "Map " + ChatColor.AQUA + a[2] + ChatColor.RED + " does not exist! Try /dr list");;
			return;
		}
		
		s.sendMessage(Vars.chatPrefix + "Map " + ChatColor.AQUA + game.getDisplayName() + ChatColor.RESET + " deleted!");
		game.delete();
	}
}
