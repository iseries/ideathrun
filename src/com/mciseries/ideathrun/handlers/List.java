package com.mciseries.ideathrun.handlers;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import com.mciseries.ideathrun.system.Game;
import com.mciseries.ideathrun.system.Vars;
import com.mciseries.ideathrun.system.files.Maps;

public class List {
	public List(CommandSender s, Command c, String[] a) {
		if(!s.hasPermission("ideathrun.player")) {
			s.sendMessage(Vars.chatPrefix + ChatColor.DARK_RED + "You do not have permission for this!");
			return;
		}
		
		boolean hasMap = false;
		StringBuilder sb = new StringBuilder();
		for(String map : Maps.mapList()) {
			hasMap = true;
			sb.append(map + " (" + new Game(map).getDisplayName() + "), ");
		}
		
		String stringList = sb.substring(0, sb.length() - 2) + ".";
		
		if(hasMap)
			s.sendMessage(Vars.chatPrefix + "Maps: " + stringList);
		else
			s.sendMessage(Vars.chatPrefix + "There are no accessible maps.");
	}
}
