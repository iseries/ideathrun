package com.mciseries.ideathrun.handlers;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import com.mciseries.ideathrun.system.Vars;

public class Help {
	public Help(CommandSender s, Command c, String[] a, boolean admin) {
		if(!admin) {
			s.sendMessage(Vars.chatPrefix + "Player Commands (/dr <command>):");
			s.sendMessage(Vars.chatPrefix + ChatColor.AQUA + "join <mapname> - " + ChatColor.RESET + "Joins the map.");
			s.sendMessage(Vars.chatPrefix + ChatColor.AQUA + "leave - " + ChatColor.RESET + "Leaves the current map.");
		}
		else {
			s.sendMessage(Vars.chatPrefix + "Admin Commands (/dr admin <command>):");
			s.sendMessage(ChatColor.AQUA + "createmap <mapname> - " + ChatColor.RESET + "Create a new map");
			s.sendMessage(ChatColor.AQUA + "deletemap <mapname> - " + ChatColor.RESET + "Delete a map");
			s.sendMessage(ChatColor.AQUA + "sethub <mapname> - " + ChatColor.RESET + "Set the Hub spawn");
			s.sendMessage(ChatColor.AQUA + "setrunnerspawn <mapname> - " + ChatColor.RESET + "Set the Runner spawn");
			s.sendMessage(ChatColor.AQUA + "setactivatorspawn <mapname> - " + ChatColor.RESET + "Set the Activator spawn");
			s.sendMessage(ChatColor.AQUA + "setbowbutton <mapname> - " + ChatColor.RESET + "Set the bow room button");
			s.sendMessage(ChatColor.AQUA + "setswordbutton <mapname> - " + ChatColor.RESET + "Set the sword room button");
			s.sendMessage(ChatColor.AQUA + "setroomsspawn <mapname> - " + ChatColor.RESET + "Set the Rooms spawn");
			s.sendMessage(ChatColor.AQUA + "setactivatorroomspawn <mapname> - " + ChatColor.RESET + "Set the activator's Room spawn");
			s.sendMessage(ChatColor.AQUA + "setonetimeuse <mapname> - " + ChatColor.RESET + "Make button one-time-use");
		}
	}
}
