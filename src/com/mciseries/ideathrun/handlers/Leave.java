package com.mciseries.ideathrun.handlers;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.mciseries.ideathrun.system.Game;
import com.mciseries.ideathrun.system.Utils;
import com.mciseries.ideathrun.system.Vars;

public class Leave {
	public Leave(CommandSender s, Command c, String[] a) {
		if(!s.hasPermission("ideathrun.player")) {
			s.sendMessage(Vars.chatPrefix + ChatColor.DARK_RED + "You do not have permission for this!");
			return;
		}
		String map = Utils.getMapPlayerIsIn((Player) s);
		if(map == null) {
			s.sendMessage(Vars.chatPrefix + ChatColor.RED + "You aren't in a game!");
			return;
		}
		
		new Game(map).playerLeave((Player) s, "left the arena");
	}
}
