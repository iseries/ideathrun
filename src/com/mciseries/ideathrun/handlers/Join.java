package com.mciseries.ideathrun.handlers;

import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.mciseries.ideathrun.system.Game;
import com.mciseries.ideathrun.system.Utils;
import com.mciseries.ideathrun.system.Vars;

public class Join {
	public Join(CommandSender s, Command c, String[] a) {
		if(!s.hasPermission("ideathrun.player")) {
			s.sendMessage(Vars.chatPrefix + ChatColor.DARK_RED + "You do not have permission for this!");
			return;
		}
		String map = Utils.getMapPlayerIsIn((Player) s);
		if(map != null) {
			s.sendMessage(Vars.chatPrefix + ChatColor.RED + "You are already in the arena " + ChatColor.AQUA + new Game(map).getDisplayName() + ChatColor.RED + "!");
			return;
		}
		if(a.length < 2) {
			s.sendMessage(Vars.chatPrefix + ChatColor.RED + "Usage: /dr join <map>");
			return;
		}
		if(new Game(a[1]).getDisplayName() == null) {
			s.sendMessage(Vars.chatPrefix + ChatColor.RED + "Map " + ChatColor.AQUA + a[1] + ChatColor.RED + " does not exist! Try /dr list");
			return;
		}
		if(new Game(a[1]).isStarted()) {
			s.sendMessage(Vars.chatPrefix + ChatColor.RED + "The map " + ChatColor.AQUA + new Game(a[1]).getDisplayName() + ChatColor.RED + " has already started! You may spectate.");
			return;
		}
		
		new Game(a[1]).playerJoin((Player) s);
		((Player) s).playSound(((Player) s).getLocation(), Sound.ENDERMAN_TELEPORT, 2, 2);
	}
}
