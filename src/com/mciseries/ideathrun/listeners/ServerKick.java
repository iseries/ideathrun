package com.mciseries.ideathrun.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerKickEvent;

import com.mciseries.ideathrun.system.files.Config;

public class ServerKick implements Listener {
	@EventHandler(priority=EventPriority.HIGH)
	public void onPlayerKickEvent(PlayerKickEvent e) {
		if(Config.dedicatedMode)
			e.setLeaveMessage("");
	}
}
