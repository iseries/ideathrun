package com.mciseries.ideathrun.listeners;

import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;

import com.mciseries.ideathrun.system.Utils;
import com.mciseries.ideathrun.system.Vars;

public class Building implements Listener {
	@EventHandler
	public void onBlockBreakEvent(BlockBreakEvent e) {
		String map = Utils.getMapPlayerIsIn(e.getPlayer());
		
		if(map != null && !e.getPlayer().isOp()) {
			e.setCancelled(true);
			e.getPlayer().sendMessage(Vars.chatPrefix + ChatColor.RED + "You can not break blocks in the arena!");
		}
	}
	
	@EventHandler
	public void onBlockPlaceEvent(BlockPlaceEvent e) {
		String map = Utils.getMapPlayerIsIn(e.getPlayer());
		
		if(map != null && !e.getPlayer().isOp()) {
			e.setCancelled(true);
			e.getPlayer().sendMessage(Vars.chatPrefix + ChatColor.RED + "You can not break blocks in the arena!");
		}
	}
}
