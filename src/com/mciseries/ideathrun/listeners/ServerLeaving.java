package com.mciseries.ideathrun.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

import com.mciseries.ideathrun.system.Game;
import com.mciseries.ideathrun.system.Utils;
import com.mciseries.ideathrun.system.files.Config;

public class ServerLeaving implements Listener {
	@EventHandler
	public void onPlayerQuitEvent(PlayerQuitEvent e) {
		String map = Utils.getMapPlayerIsIn(e.getPlayer());
		
		if(Config.dedicatedMode)
			e.setQuitMessage("");
		if(map != null) {
			new Game(map).playerLeave(e.getPlayer(), "left the server");
		}
	}
}
