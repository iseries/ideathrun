package com.mciseries.ideathrun.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

import com.mciseries.ideathrun.system.Game;
import com.mciseries.ideathrun.system.Utils;
import com.mciseries.ideathrun.system.Vars;

public class Death implements Listener {
	@EventHandler
	public void onPlayerDeathEvent(PlayerDeathEvent e) {
		String map = Utils.getMapPlayerIsIn(e.getEntity());
		
		if(map != null) {
			e.getDrops().clear();
			new Game(map).playerLeave(e.getEntity(), "died");
			e.setDeathMessage("");
			Vars.respawnWithGame.put(e.getEntity().getName(), map);
		}
	}
}
