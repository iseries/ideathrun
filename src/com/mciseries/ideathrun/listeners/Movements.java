package com.mciseries.ideathrun.listeners;

import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

import com.mciseries.ideathrun.system.Game;
import com.mciseries.ideathrun.system.Utils;
import com.mciseries.ideathrun.system.files.Config;

public class Movements implements Listener {
	@EventHandler
	public void onPlayerMoveEvent(PlayerMoveEvent e) {
		String map = Utils.getMapPlayerIsIn(e.getPlayer());
		
		if(map != null) {
			Game game = new Game(map);
			
			Location playerLoc = e.getPlayer().getLocation();
			playerLoc.setY(playerLoc.getY() - 1);
			Block below = playerLoc.getBlock();
			
			if(below.getTypeId() == Config.floorId) {
				if(!e.getPlayer().getName().equalsIgnoreCase(game.getActivator())) {
					game.playerLeave(e.getPlayer(), "hit the arena floor");
				}
			}
		}
	}
}
