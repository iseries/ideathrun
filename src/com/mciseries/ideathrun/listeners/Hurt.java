package com.mciseries.ideathrun.listeners;

import org.bukkit.ChatColor;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

import com.mciseries.ideathrun.system.Game;
import com.mciseries.ideathrun.system.Utils;
import com.mciseries.ideathrun.system.Vars;

public class Hurt implements Listener {
	@EventHandler
	public void onEntityDamageByEntityEvent(EntityDamageByEntityEvent e) {
		if(e.getEntityType() == EntityType.PLAYER && e.getDamager().getType() == EntityType.PLAYER) {
			String map = Utils.getMapPlayerIsIn((Player) e.getEntity());
			
			if(map != null) {
				Game g = new Game(map);
				if(!g.getActivator().equalsIgnoreCase(((Player) e.getEntity()).getName()) && !g.getActivator().equalsIgnoreCase(((Player) e.getDamager()).getName())) {
					((Player) e.getDamager()).sendMessage(Vars.chatPrefix + ChatColor.RED + "You cannot hurt your teammates!");
					e.setCancelled(true);
				}
			}
		}
	}
}
