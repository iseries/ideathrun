package com.mciseries.ideathrun.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import com.mciseries.ideathrun.system.Game;
import com.mciseries.ideathrun.system.Scoreboard;
import com.mciseries.ideathrun.system.extras.Bungee;
import com.mciseries.ideathrun.system.files.Config;
import com.mciseries.ideathrun.system.files.Maps;

public class ServerJoining implements Listener {
	@EventHandler
	public void onPlayerJoinEvent(PlayerJoinEvent e) {
		Scoreboard.init();
		
		if(Config.dedicatedMode) {
			String[] maps = Maps.mapList();
			if(new Game(maps[0]).isStarted()) {
				e.setJoinMessage("");
				if(!Config.bungeeServerOnRoundEnd.equalsIgnoreCase("none"))
					Bungee.send(e.getPlayer());
				return;
			}
			new Game(maps[0]).playerJoin(e.getPlayer());
		}
	}
}
