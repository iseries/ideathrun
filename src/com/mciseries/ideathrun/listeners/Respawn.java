package com.mciseries.ideathrun.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerRespawnEvent;

import com.mciseries.ideathrun.system.Game;
import com.mciseries.ideathrun.system.Vars;
import com.mciseries.ideathrun.system.files.Inventories;

public class Respawn implements Listener {
	@EventHandler
	public void onPlayerRespawnEvent(PlayerRespawnEvent e) {
		if(Vars.respawnWithGame.containsKey(e.getPlayer().getName())) {
			e.setRespawnLocation(new Game(Vars.respawnWithGame.get(e.getPlayer().getName())).getHub());
			Inventories.restoreInv(e.getPlayer());
			Vars.respawnWithGame.remove(e.getPlayer().getName());
		}
	}
}
