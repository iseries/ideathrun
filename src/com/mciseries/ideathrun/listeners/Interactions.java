package com.mciseries.ideathrun.listeners;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import com.mciseries.ideathrun.system.Game;
import com.mciseries.ideathrun.system.Utils;
import com.mciseries.ideathrun.system.Vars;

public class Interactions implements Listener {
	@EventHandler
	public void onPlayerInteractEvent(PlayerInteractEvent e) {
		String map = Utils.getMapPlayerIsIn(e.getPlayer());
		
		if(e.getAction() == Action.LEFT_CLICK_BLOCK || e.getAction() == Action.RIGHT_CLICK_BLOCK) {
			if(Vars.oneTimeUseSelectors.containsKey(e.getPlayer().getName())) {
				if(e.getClickedBlock().getType() == Material.STONE_BUTTON || e.getClickedBlock().getType() == Material.WOOD_BUTTON) {
					new Game(Vars.oneTimeUseSelectors.get(e.getPlayer().getName())).setOneTimeUse(e.getClickedBlock().getLocation());
					e.getPlayer().sendMessage(Vars.chatPrefix + "This button is now one-time use!");
					Vars.oneTimeUseSelectors.remove(e.getPlayer().getName());
					e.setCancelled(true);
				}
			}
			else if(Vars.bowButtonSelectors.containsKey(e.getPlayer().getName())) {
				if(e.getClickedBlock().getType() == Material.STONE_BUTTON || e.getClickedBlock().getType() == Material.WOOD_BUTTON) {
					new Game(Vars.bowButtonSelectors.get(e.getPlayer().getName())).setBowButton(e.getClickedBlock().getLocation());
					e.getPlayer().sendMessage(Vars.chatPrefix + "This button is now the bow button!");
					Vars.bowButtonSelectors.remove(e.getPlayer().getName());
					e.setCancelled(true);
				}
			}
			else if(Vars.swordButtonSelectors.containsKey(e.getPlayer().getName())) {
				if(e.getClickedBlock().getType() == Material.STONE_BUTTON || e.getClickedBlock().getType() == Material.WOOD_BUTTON) {
					new Game(Vars.swordButtonSelectors.get(e.getPlayer().getName())).setSwordButton(e.getClickedBlock().getLocation());
					e.getPlayer().sendMessage(Vars.chatPrefix + "This button is now the sword button!");
					Vars.swordButtonSelectors.remove(e.getPlayer().getName());
					e.setCancelled(true);
				}
			}
			else if(map != null) {
				Game game = new Game(map);
				if(game.isBowButton(e.getClickedBlock().getLocation())) {
					Player activator = e.getPlayer().getServer().getPlayer(game.getActivator());
					e.getPlayer().setItemInHand(new ItemStack(Material.BOW, 1));
					e.getPlayer().getInventory().addItem(new ItemStack(Material.ARROW, 128));
					e.getPlayer().teleport(game.getRoomsSpawn());
					
					if(!Vars.gamesWithTeleportedActivators.contains(game.getName())) {
						activator.setItemInHand(new ItemStack(Material.BOW, 1));
						activator.getInventory().addItem(new ItemStack(Material.ARROW, 128));
						activator.teleport(game.getActivatorRoomSpawn());
						Vars.gamesWithTeleportedActivators.add(game.getName());
					}
					
					e.setCancelled(true);
				}
				else if(game.isSwordButton(e.getClickedBlock().getLocation())) {
					Player activator = e.getPlayer().getServer().getPlayer(game.getActivator());
					e.getPlayer().setItemInHand(new ItemStack(Material.STONE_SWORD, 1));
					e.getPlayer().teleport(game.getRoomsSpawn());
					
					if(!Vars.gamesWithTeleportedActivators.contains(game.getName())) {
						activator.setItemInHand(new ItemStack(Material.STONE_SWORD, 1));
						activator.teleport(game.getActivatorRoomSpawn());
						Vars.gamesWithTeleportedActivators.add(game.getName());
					}
					
					e.setCancelled(true);
				}
				else if(game.isOneTimeUse(e.getClickedBlock().getLocation())) {
					if(Vars.oneTimeUseUses.contains(Utils.serializeBlockLocation(e.getClickedBlock().getLocation()))) {
						e.getPlayer().sendMessage(Vars.chatPrefix + "This button is one-time use only!");
						e.setCancelled(true);
					}
					else {
						Vars.oneTimeUseUses.add(Utils.serializeBlockLocation(e.getClickedBlock().getLocation()));
					}
				}
			}
		}
	}
}
